const usersStore = {
    state: () => ({
        currentUser: null,
        selectedUser: null,
        userNotifications: [],
    }),
    mutations: {
        setCurrentUser(state, payload) {
            state.currentUser = payload.user;
        },
        setSelectedUser(state, payload) {
            state.selectedUser = payload.user;
        },
        addNotification(state, payload) {
            state.userNotifications.push(payload.notification);
        },
        removeNotification(state, payload) {
            state.userNotifications.splice(payload.index, 1)
        },
    },
    actions: {
        loadCurrentUser(context) {
            axios.get('/api/users/current')
                .then((response) => {
                    if(!context.state.currentUser) {
                        Echo.private('App.Models.User.' + response.data.data.id)
                            .notification((notification) => {
                                if(!notification.message) return;

                                this.dispatch('loadCurrentUser')
                                context.commit('addNotification', { notification: notification.message })
                            });
                    }

                    context.commit('setCurrentUser', {user: response.data.data});
                });
        },
        logout() {
            axios.get('/api/logout')
                .then((response) => {
                    localStorage.removeItem('authToken');
                    window.location.href = '/';
                });
        },
    },
    getters: {

        currentUser(state) {
            return state.currentUser;
        },

        getUserNotifications(state) {
            return state.userNotifications;
        },

    },
}
export default usersStore;

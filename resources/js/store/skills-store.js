export default {
    state: () => ({
        skills: [],
    }),
    mutations: {
        setSkills (state, payload) {
            state.skills = payload.skills;
        },
    },
    actions: {
        loadSkills(context) {
            axios.get('/api/skills')
                .then((response) => {
                    context.commit('setSkills', {skills: response.data.data});
                });
        }
    },
    getters: {
        getSkills (state) {
            return state.skills;
        },
    },
}

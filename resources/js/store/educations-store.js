const educationsStore = {
    state: () => ({
        educations: [],
    }),
    mutations: {
        setEducations(state, payload) {
            state.educations = payload.educations;
        },
    },
    actions: {
        loadEducations(context) {
            axios.get('/api/educations').then((response) => {
                context.commit('setEducations', {educations: response.data.data});
            });
        },
    },
    getters: {
        getEducations(state) {
            return state.educations;
        }
    },
}
export default educationsStore;

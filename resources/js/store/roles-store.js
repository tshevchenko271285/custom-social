export default {
    state: () => ({
        roles: [],
    }),
    mutations: {
        setRoles (state, payload) {
            state.roles = payload.roles;
        },
    },
    actions: {
        loadRoles(context) {
            axios.get('/api/roles')
                .then((response) => {
                    context.commit('setRoles', {roles: response.data.data});
                });
        }
    },
    getters: {
        getRoles (state) {
            return state.roles;
        },
    },
}

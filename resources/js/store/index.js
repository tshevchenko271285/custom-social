import Vue from 'vue';
import Vuex from 'vuex';
import usersStore from './users-store';
import educationsStore from './educations-store';
import rolesStore from './roles-store';
import postsStore from './posts-store';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {

    },

    actions: {

    },

    mutations: {

    },

    getters: {

    },

    modules: {
        users: usersStore,
        educations: educationsStore,
        roles: rolesStore,
        // posts: postsStore,
    }
});

export default store;

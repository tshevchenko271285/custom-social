const usersStore = {
    state: () => ({
        posts: [],
        pagination: {},
        page: 1,
        currentPostId: null,
        currentPost: null,
    }),
    mutations: {
        setPosts(state, payload) {
            state.posts = payload.posts;
        },
        setPagination(state, payload) {
            state.pagination = payload.pagination;
        },
        setPostsPage(state, payload) {
            state.page = payload.page;
        },
        setCurrentPostId(state, payload) {
            state.currentPostId = payload.id;
        },
        setCurrentPost(state, payload) {
            state.currentPost = payload.post;
        },
    },
    actions: {
        loadAllPosts(context) {
            axios.get('/api/posts', {
                params: {
                    page: context.state.page,
                },
            })
            .then((response) => {
                context.commit('setPosts', {posts: response.data.data});
                context.commit('setPagination', {pagination: response.data.meta});
                context.commit('setPostsPage', {page: response.data.meta.current_page});
            });

        },
        loadCurrentPost (context) {
            if(!context.state.currentPostId)
                return;

            axios.get('/api/posts/' + context.state.currentPostId)
            .then((response) => {
                context.commit('setCurrentPost', {post: response.data.data});
            })
            .catch((error) => {
                console.log(error)
            });
        },
    },
    getters: {
        getPosts(state) {
            return state.posts;
        },
        getPostsPagination(state) {
            return state.pagination;
        },
        getPostsPage(state) {
            return state.page;
        },
        getCurrentPost(state) {
            return state.currentPost;
        }
    },
}
export default usersStore;

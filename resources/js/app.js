/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./scripts');
import store from './store/index'


window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

import customSocialHelpers from './plugins/customSocialHelpers';
Vue.use(customSocialHelpers);

// import headerProfile from './components/header-profile';
// Vue.component('header-profile', headerProfile);

import mainMenu from './components/main-menu';
Vue.component('main-menu', mainMenu);

import portfolioList from './components/portfolio-list';
Vue.component('portfolio-list', portfolioList);

import portfolioDetailsModal from "./components/portfolio-details-modal";
Vue.component('portfolio-details-modal', portfolioDetailsModal);

import authLogin from './components/auth/auth-login.vue';
Vue.component('auth-login', authLogin);

import authRegister from './components/auth/auth-register.vue';
Vue.component('auth-register', authRegister);

import postForm from './components/post/post-form';
Vue.component('post-form', postForm);

import postsList from './components/post/posts-list';
Vue.component('posts-list', postsList);

import postShow from './components/post/post-show';
Vue.component('post-show', postShow);

import notificationsList from './components/notifications-list';
Vue.component('notifications-list', notificationsList);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    store,

    beforeMount() {
        if(this.$checkAuth()) {
            this.$store.dispatch('loadCurrentUser');
        }
    },

});

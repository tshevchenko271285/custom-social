export default {
    install (Vue, options) {

        Vue.prototype.$hasRole = function (searchRole) {
            const result = this.$store.state.users.currentUser.roles.filter( role => role.slug === searchRole );

            return Boolean(result.length);
        }

        Vue.prototype.$checkAuth = function() {
            return Boolean(
                window.axios.defaults.headers.common['Authorization']
                && window.axios.defaults.headers.common['Authorization'].length
            );
        }
    }
};


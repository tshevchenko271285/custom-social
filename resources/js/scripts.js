// window.addEventListener('load', function () {
//     tinymce.init({
//         selector: '.tinyeditor'
//     });
// });

window.addEventListener('load', function () {
    document.addEventListener('click', function (e) {
        if(e.target.closest('.post-comment__edit')) {
            e.target.closest('.post-comment')
                .querySelector('.post-comment__form')
                .classList.toggle('post-comment__form--show');
        }
    });
});

@extends('app')

@section('content')
    <div class="container">

        <h1 class="my-5 text-center">{{ __('Update news') }}</h1>

        @include('vue-templates.post.post-form')
        <post-form v-bind:post-id="{{ $postId }}"></post-form>

        {{--        @include('post.news-form')--}}

{{--        <form action="{{ route('posts.destroy', $post) }}" method="POST" class="text-right">--}}
{{--            @csrf--}}
{{--            @method('delete')--}}
{{--            <input--}}
{{--                type="submit"--}}
{{--                onclick="confirm('{{ __('The post will be deleted?') }}')"--}}
{{--                class="btn btn-danger"--}}
{{--                value="{{ __('Remove') }}"--}}
{{--            />--}}
{{--        </form>--}}
    </div>
@endsection

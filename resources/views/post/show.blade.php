@extends('app')

@section('content')

    @include('vue-templates.post.post-show')
    <post-show :post-id="{{ $postId }}"></post-show>

    @include('vue-templates.comment.comments-list')

@endsection

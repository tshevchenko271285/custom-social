@include('partial.alert_errors')

<form
    action="{{ !empty($post) ? route('posts.update', $post) : route('posts.store') }}"
    method="POST"
    class="news-form"
>

    @csrf

    @isset($post)
        @method('PATCH')
    @endisset

    <input
        type="text"
        name="title"
        value="@isset($post){{ $post->title }}@endisset"
        class="news-form__input news-form__input--title"
    />

    <textarea
        name="content"
        class="news-form__input news-form__textarea tinyeditor"
        rows="20"
    >
        @isset($post){{ $post->content }}@endisset
    </textarea>

    <button type="submit" class="btn btn-primary news-form__action">{{ __('Save') }}</button>

</form>

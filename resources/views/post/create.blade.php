@extends('app')

@section('content')
    <div class="container">

        <h1 class="my-5 text-center">{{ __('Create news') }}</h1>

        @include('vue-templates.post.post-form')
        <post-form></post-form>

    </div>
@endsection

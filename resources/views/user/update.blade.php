@extends('app')

@section('content')
    <div class="container" style="display:flex; align-items:center; justify-content:center; flex-direction: column; min-height: 90vh">

        @include('partial.alert_errors')

        <form method="POST" action="{{ route('users.update', $user) }}" class="form" style="max-width: 900px;margin:auto;width: 100%;">
            @csrf
            @method('PUT')
            <h1 class="text-center">Edit profile</h1>
            <div class="row">
                <div class="col-9">
                    <div class="mb-3">
                        <label for="inputName" class="form-label">Name</label>
                        <input
                            type="text"
                            class="form-control"
                            id="inputName"
                            name="user[name]"
                            value="{{$user->name ?? old('user.name')}}"
                        />
                    </div>
                    <div class="mb-3">
                        <label for="inputEmail" class="form-label">Email address</label>
                        <input
                            type="email"
                            class="form-control"
                            id="inputEmail"
                            name="user[email]"
                            value="{{$user->email ?? old('user.email')}}"
                        />
                    </div>

                    <div class="mb-3">
                        <label for="education" class="form-label">Education</label>
                        <select name="profile[education_id]" id="education" class="form-select">
                            @foreach($educations as $education)
                                <option
                                    value="{{ $education->id }}"
                                    @if($user->profile->education->id === $education->id) selected @endif>
                                    {{ $education->title }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="mb-3">
                        <label for="experience" class="form-label">Experience</label>
                        <input
                            type="text"
                            class="form-control"
                            id="education"
                            name="profile[experience]"
                            value="{{$user->profile ? $user->profile->experience : old('profile.experience')}}"
                        />
                    </div>

                    @if(count($roles))
                        <label class="form-label">Roles</label>
                        <div class="">
                            @foreach($roles as $role)
                                <div class="col form-check">
                                    <label for="role-{{ $role->slug }}" class="form-check-label">{{ $role->title }}</label>
                                    @if(\App\Facades\CheckUser::hasRole(['admin']))
                                        <input
                                            type="checkbox"
                                            class="form-check-input"
                                            id="role-{{ $role->slug }}"
                                            name="roles[]"
                                            value="{{ $role->id }}"
                                            {{ \App\Facades\CheckUser::hasRole([$role->slug], $user) ? 'checked' : '' }}
                                        />
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    @endif


                    <hr class="mt-5 mb-5">

                    <div class="mb-3">
                        <label for="inputPassword" class="form-label">Password</label>
                        <input
                            type="password"
                            class="form-control"
                            id="inputPassword"
                            name="user[password]"
                            value=""
                        />
                    </div>
                    <div class="">
                        <label for="inputPassword2" class="form-label">Confirm password</label>
                        <input
                            type="password"
                            class="form-control"
                            id="inputPassword2"
                            name="user[password_confirmation]"
                            value=""
                        />
                    </div>
                </div>
                <div class="col-3">
                    <select name="skills[]" class="form-select h-100" multiple>
                        @foreach($skills as $skill)
                            <option
                                value="{{$skill->id}}"
                                {{in_array($skill->id, $user->skills->pluck('id')->toArray()) ? 'selected' : ''}}
                            >
                                {{$skill->title}}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="mt-3 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
                <span id="userRemove" class="btn btn-danger ml-5">Remove</span>
            </div>
        </form>
        <script>
            window.addEventListener('load', function (){

                document.addEventListener('click', function (e){
                    const removeBtn = e.target.closest('#userRemove');
                    if(removeBtn) {
                        if(window.confirm('Удалить {{$user->name}}?')) {

                            const xhr = new XMLHttpRequest();
                            const data = new FormData();
                            data.append('_token', '{{csrf_token()}}')
                            data.append('_method', 'DELETE')
                            xhr.open("DELETE", '{{route('users.destroy', $user)}}');

                            xhr.setRequestHeader("X-CSRF-TOKEN", "{{csrf_token()}}");

                            xhr.onreadystatechange = function () {
                                if (xhr.readyState === 4 && xhr.status === 200) {
                                    document.location.href = xhr.responseText;
                                }
                            };

                            xhr.send(data);
                        }
                    }
                });
            });
        </script>
    </div>
@endsection

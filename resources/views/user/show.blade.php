@extends('app')

@section('content')

    <div class="container" style="display:flex; align-items:center; justify-content:center; flex-direction: column; min-height: 90vh">
        <h1><a href="{{route('users.edit', $user)}}">{{$user->name}}</a></h1>
        <h2>{{$user->email}}</h2>

        <ul class="w-50 mt-5 list-group">
            <li class="list-group-item">Education: {{$user->profile->education}}</li>
            <li class="list-group-item">Experience: {{$user->profile->experience}}</li>
        </ul>
    </div>

@endsection

@extends('app')

@section('content')
    <portfolio-list></portfolio-list>
    @include('vue-templates/portfolio-list')
@endsection

@extends('app')

@section('content')
    <div class="container" style="display:flex; align-items:center; justify-content:center; flex-direction: column; min-height: 90vh">

        @include('partial.alert_errors')

        <form method="POST" action="{{ route('users.store') }}" class="form" style="max-width: 600px;margin:auto; width: 100%;">
            @csrf
            <h1 class="text-center">Create user</h1>
            <div class="mb-3">
                <label for="inputName" class="form-label">Name</label>
                <input type="text" class="form-control" id="inputName"  name="name" value="{{old('name')}}">
            </div>
            <div class="mb-3">
                <label for="inputEmail" class="form-label">Email address</label>
                <input type="email" class="form-control" id="inputEmail" name="email" value="{{old('email')}}">
            </div>
            <div class="mb-3">
                <label for="inputPassword" class="form-label">Password</label>
                <input type="password" class="form-control" id="inputPassword" name="password" value="{{old('password')}}">
            </div>
            <div class="mb-3">
                <label for="inputPassword2" class="form-label">Confirm password</label>
                <input type="password" class="form-control" id="inputPassword2" name="password_confirmation" value="{{old('password_confirmation')}}">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

@endsection

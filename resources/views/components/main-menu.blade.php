<ul class="d-flex align-items-center justify-content-center" style="list-style: none">

    <li class="p-3">
        <a href="{{route('posts.index')}}">{{ __('News') }}</a>
    </li>

    @if(\Auth::guard('api')->user() && \Auth::guard('api')->user()->can('create', \App\Models\Post::class))
{{--    @can('create', \App\Models\Post::class)--}}
        <li class="p-3">
            <a href="{{route('posts.create')}}">{{ __('Create News') }}</a>
        </li>
    @endif
{{--    @endcan--}}

    <li class="p-3">
        <a href="{{route('users.index')}}">{{ __('Users') }}</a>
    </li>

    @if(!\Auth::guard('api')->check())
{{--    @guest--}}
        <li class="p-3">
            <a href="{{route('login')}}">{{ __('Login') }}</a>
        </li>
{{--    @endguest--}}
    @endif

</ul>

<div class="post-comment {{ !$comment->approved ? 'post-comment--not-approved' : '' }}">
    <div class="post-comment__header">
        <div class="post-comment__date">{{ $comment->created_at }}</div>
    </div>
    <div class="post-comment__body">{{ $comment->text }}</div>
    <div class="post-comment__footer">
        <div class="post-comment__author">
            {{$comment->user->name}}
        </div>

        @auth @can('update', $comment)
            <span class="post-comment__edit"><i class="fas fa-pencil-alt"></i></span>
        @endcan @endif

        @auth @can('delete', $comment)
            <form method="POST" action="{{ route('comments.destroy', $comment) }}" class="post-comment__remove">
                @csrf
                @method('delete')
                <button type="submit" onclick="return confirm('{{__('The comment will be deleted')}}')">
                    <i class='far fa-trash-alt'></i>
                </button>
            </form>

            <form
                action="{{ $comment->approved ? route('comments.deny') : route('comments.approve') }}"
                method="POST"
                class="post-comment__remove"
            >
                @csrf
                @method('patch')
                <input type="hidden" name="comment" value="{{ $comment->id }}">
                <button type="submit">
                    @if($comment->approved)
                        <i class="far fa-thumbs-down"></i>
                    @else
                        <i class="far fa-thumbs-up"></i>
                    @endif
                </button>
            </form>

        @endcan @endif

    </div>
    @auth @can('update', $comment)
        <div class="post-comment__form">
            @include('comment.form', ['post' => $post, 'comment' => $comment])
        </div>
    @endcan @endif
</div>

<form
    method="POST"
    action="{{!empty($comment) ? route('comments.update', $comment) : route('comments.store')}}"
    class="post-comments-form"
>
    @csrf
    @isset($comment)
        @method('PATCH')
    @endisset
    <input type="hidden" name="post_id" value="{{ $post->id }}">
{{--    <input type="hidden" name="user_id" value="{{ !empty($comment) ? $comment->user->id : '1' }}">--}}
    <textarea class="post-comments-form__textarea" name="text" cols="30" rows="5">{{ !empty($comment) ? $comment->text : '' }}</textarea>
    <div class="post-comments-form__actions">
        <button type="submit" class="btn btn-outline-primary post-comments-form__action">{{ __('Send') }}</button>
    </div>

</form>

{{--@isset($comment)--}}
{{--    <form action="#asd">--}}
{{--        <input--}}
{{--            type="submit"--}}
{{--            class="btn btn-outline-danger post-comments-form__action"--}}
{{--            onclick="confirm('{{ __('The comment will be deleted') }}')"--}}
{{--            value="{{ __('Remove') }}"--}}
{{--        >--}}
{{--    </form>--}}
{{--@endisset--}}

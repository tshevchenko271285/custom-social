@extends('app')

@section('content')

    <div class="container">

        <auth-login></auth-login>

        @include('vue-templates.auth-login')

    </div>

@endsection


@extends('app')

@section('content')

    <div class="container">

        <auth-register></auth-register>

        @include('vue-templates.auth-register')

    </div>

@endsection

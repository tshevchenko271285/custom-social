<script type="text/x-template" id="post-form-template">
    <form
        action="{{ !empty($postId) ? route('api.posts.update', $postId) : route('api.posts.store') }}"
        class="news-form"
        @submit.prevent="submitForm"
    >
        <span v-if="errors.credential" class="invalid-feedback d-block" role="alert">
            <strong>{# errors.credential #}</strong>
        </span>

        <input
            v-model="title"
            type="text"
            name="title"
            class="news-form__input news-form__input--title"
            :class="{'is-invalid': errors.title}"
        />
        <span v-if="errors.title" class="invalid-feedback" role="alert">
            <strong>{# errors.title[0] #}</strong>
        </span>

        <editor
            v-model="content"
            api-key="no-api-key"
            :init="{
                height: 500,
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table paste code help wordcount'
                ],
                toolbar:
                    'undo redo | formatselect | bold italic backcolor | \
                    alignleft aligncenter alignright alignjustify | \
                    bullist numlist outdent indent | removeformat | help'
            }"
        />
        <span v-if="errors.content" class="invalid-feedback" role="alert">
            <strong>{# errors.content[0] #}</strong>
        </span>

        <button type="submit" class="btn btn-primary news-form__action">{{ __('Save') }}</button>

    </form>
</script>

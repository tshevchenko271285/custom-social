<script type="text/x-template" id="posts-show-template">
    <div class="container">

        <div class="single-post" v-if="post">

            <h1 class="text-center mb-5 single-post__title">
                {# post.title #}
                <a v-if="post.url_edit" :href="post.url_edit" class="single-post__edit">
                    <i class="fas fa-pencil"></i>
                </a>
            </h1>

            <div class="d-flex justify-content-between mb-5 py-2 single-post__header">

                <div class="single-post__author">
                    {# post.user.name #}
                    <br>
                    {# post.user.email #}
                </div>

                <div class="single-post__date">
                    {# post.created_at #} {{ __('updated') }} ({# post.updated_at #})
                </div>

            </div>

            <div class="single-post__content" v-html="post.content"></div>

        </div>

        <comments-list></comments-list>
    </div>
</script>

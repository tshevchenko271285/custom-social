<script type="text/x-template" id="posts-list-template">
    <div>
        <div class="posts">

            <div v-for="post in posts" class="post">

                <div class="post__header">

                    <h2 class="post__title">
                        <a :href="post.url">
                            {# post.title #}
                        </a>
                    </h2>

                    <a v-if="post.url_edit" :href="post.url_edit" class="single-post__edit">
                        <i class="fas fa-pencil"></i>
                    </a>

                </div>

                <div class="post__body">
                    {# post.description #}
                </div>

                <div class="post__footer">
                    <div class="post__date">{# post.created_at #}</div>
                    <div class="post__author">
                        {# post.user.name #}
                        <br /><small>{# post.user.email #}</small>
                    </div>
                </div>

            </div>

        </div>
        <div class="row">
            <nav aria-label="Page navigation example">
                <ul class="pagination d-flex justify-content-center">
                    <li
                        v-for="link of pagination.links"
                        :class="{'page-item': true, 'disabled': !link.url, 'active': link.active}"
                    >
                        <a
                            class="page-link"
                            :href="link.url ? link.url : '#'"
                            v-html="link.label"
                            @click.prevent="listenSelectPagination(link.label)"
                        ></a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</script>

<script type="text/x-template" id="comments-list-template">
    <div class="post-comments" v-if="post">
        <h2>{{ __('Comments:') }}</h2>

        <div v-if="$checkAuth()" class="p-3 mb-5">
            <comment-form></comment-form>
        </div>

        <div
            v-for="comment in post.comments"
            :key="comment.id"
            :class="{'post-comment--not-approved' : !comment.approved}"
            class="post-comment"
        >
            <div class="post-comment__header">

                <div class="post-comment__date">{# comment.created_at #}</div>

                <div class="post-comment__admin-panel">
                    <a href="#"
                       v-if="comment.approve_url"
                       class="btn btn-outline-primary ml-auto mr-2"
                       @click.prevent="approveComment(comment)"
                    >
                        <i
                            class="far fa-thumbs-down"
                            :class="{
                            'fa-thumbs-down': comment.approved,
                            'fa-thumbs-up': !comment.approved,
                        }"
                        ></i>
                    </a>

                    <a
                        href="#"
                        v-if="comment.remove_url"
                        class="btn btn-outline-danger ml-0 mr-0"
                        @click.prevent="removeComment(comment)"
                    >
                        <i class="fas fa-trash"></i>
                    </a>
                </div>

            </div>
            <div class="post-comment__body">{# comment.text #}</div>
            <div class="post-comment__footer">
                <div class="post-comment__author">
                    {# comment.user.name #}
                </div>
            </div>

            <comment-form :comment="comment" v-if="comment.update_url"></comment-form>

        </div>


    </div>
</script>

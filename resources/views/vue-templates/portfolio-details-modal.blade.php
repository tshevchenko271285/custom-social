<script type="text/x-template" id="portfolio-details-modal">
    <div v-if="user" class="portfolio-details-modal">
        <div class="portfolio-details-modal__substrate" @click="closeModal"></div>
        <div class="portfolio-details-modal__window">
            <div class="portfolio-details-modal__close" @click="closeModal">x</div>
            <div class="portfolio-details-modal-avatar">
                <div class="portfolio-details-modal-avatar__photo">
                    <img v-if="user.avatar" :src="user.avatar" :alt="user.name" />
                </div>
                <div class="portfolio-details-modal-avatar__actions">
                    <div class="portfolio-details-modal-avatar__error">
                        <span v-if="this.errors && errors['avatar']" class="portfolio-details-modal__error">
                            {# Array.isArray(errors['avatar']) ? errors['avatar'][0] : errors['avatar'] #}
                        </span>
                    </div>
                    <span
                        v-if="user.avatar"
                        class="btn btn-danger portfolio-details-modal-avatar__action"
                        @click="removeAvatar()"
                    >{{ __('Remove') }}</span>
                    <span
                        class="btn btn-success portfolio-details-modal-avatar__action"
                        :class="{'mx-auto': !user.avatar}"
                        @click="$refs['avatar'].click()"
                    >{{ __('Upload') }}</span>
                    <input type="file" ref="avatar" class="d-none" @change="changeAvatar()"/>
                </div>
            </div>
            <div class="portfolio-details-modal__content">
                <h2 class="mb-3 text-center">{#user.name#}</h2>
                <div class="mb-3">
                    <label for="userName" class="form-label">{{ __('Name') }}</label>
                    <input v-model="user.name" type="text" class="form-control" id="userName">
                    <span v-if="this.errors && errors['user.name']" class="portfolio-details-modal__error">
                        {# errors['user.name'][0] #}
                    </span>
                </div>
                <div class="mb-3">
                    <label for="userEmail" class="form-label">{{ __('Email') }}</label>
                    <input v-model="user.email" type="email" class="form-control" id="userEmail">
                    <span v-if="this.errors && errors['user.email']" class="portfolio-details-modal__error">
                        {# errors['user.email'][0] #}
                    </span>
                </div>
                <div v-if="educations" class="row mb-3">
                    <div class="col">
                        <label class="form-label">{{ __('Education') }}</label>
                        <select class="form-select" v-model="user.education">
                            <option
                                v-for="education in educations"
                                :key="education.id"
                                :value="education"
                            >
                                {# education.title #}
                            </option>
                        </select>
                    </div>
                    <div class="col">
                        <label for="userExperience" class="form-label">{{ __('Experience') }}</label>
                        <input v-model="user.experience" type="number" class="form-control" id="userExperience">
                        <span v-if="this.errors && errors['profile.experience']" class="portfolio-details-modal__error">
                            {# errors['profile.experience'][0] #}
                        </span>
                    </div>
                </div>

                <div v-if='(user.roles.length || $hasRole("admin"))' class="mb-3">
                    <div v-for="role in roles" :key="role.id" class="form-check form-check-inline">
                        <input
                            class="form-check-input"
                            type="checkbox" :id="'role' + role.id"
                            :value="role"
                            v-model="user.roles"
                            :disabled="!$hasRole('admin')"
                        >
                        <label class="form-check-label" :for="'role' + role.id">{# role.title #}</label>
                    </div>
                </div>

                <div class="text-center">
                    <span v-if="this.errors && errors['auth']" class="d-block mb-3 portfolio-details-modal__error">
                        {# errors['auth'] #}
                    </span>
                    <button type="button" class="btn btn-primary" @click="saveModal">{{ __('Save') }}</button>
                </div>
            </div>
            <div class="portfolio-details-modal-skills">
                <span
                    v-for="skill in skills"
                    class="portfolio-details-modal-skill"
                    :class="{'portfolio-details-modal-skill--active': userSkillsId.includes(skill.id)}"
                    @click="selectSkill(skill)"
                >
                    {#skill.title#}
                </span>
            </div>
        </div>
    </div>
</script>

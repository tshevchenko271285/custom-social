<script type="text/x-template" id="template-portfolio-list">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <filter-user-skills
                    @listen-selected-skills="listenSelectedSkills"
                    :skills="skills"
                />
            </div>
        </div>

        <table class="table portfolio-list">
            <thead>
                <tr>
                    <th class="sortingColumn" scope="col" @click="sort('id')">{{__('#')}}</th>
                    <th class="sortingColumn" scope="col" @click="sort('name')">{{__('Name')}}</th>
                    <th class="sortingColumn" scope="col" @click="sort('email')">{{__('Email')}}</th>
                    <th class="" scope="col">
                        <filter-user-educations
                            :educations="educations"
                            @listen-selected-education="listenSelectedEducation"
                        />
                    </th>
                    <th scope="col">{{__('Experience')}}</th>
                    <th class="text-center" scope="col">{{__('Skills')}}</th>
                    <th class="" scope="col">
                        <div class="row">
                            <label class="col-md-3">{{__('Start Date:')}}</label>
                            <div class="col-md-9">
                                <input
                                    @change="listenSelectedStartDate($event.target.value)"
                                    type="date"
                                    class="form-control"
                                />
                            </div>
                            <label class="col-md-3">{{__('End Date:')}}</label>
                            <div class="col-md-9">
                                <input
                                    @change="listenSelectedEndDate($event.target.value)"
                                    type="date"
                                    class="form-control"
                                />
                            </div>
                        </div>
                    </th>

                    <th class="text-center" scope="col"></th>

                </tr>
            </thead>
            <tbody>
                <tr
                    v-for="user in users"
                    v-show="user.show"
                    class="portfolio-list-item"
                >
                    <th scope="row">{# user.id #}</th>
                    <td>{# user.name #}</td>
                    <td>{# user.email #}</td>
                    <td>{# user.education ? user.education.title : '' #}</td>
                    <td>{# user.experience #}</td>
                    <td>
                        <ul v-if="user.skills" class="user-skills">
                            <li
                                v-for="skill in user.skills"
                                :class="{'user-skills__item--active': skill.active}"
                                class="user-skills__item">
                                {# skill.title #}
                            </li>
                        </ul>
                    </td>
                    <td>{# user.created_at #}</td>

                    <td class="text-center">
                        <span
                            v-if="user.hasEdit"
                            @click="listenSelectedUser(user)"
                            class="portfolio-list-item__action"
                        >{{__('Edit')}}</span>
                    </td>

                </tr>
            </tbody>
        </table>
        <div class="row">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li
                        v-for="link of pagination.links"
                        :class="{'page-item': true, 'disabled': !link.url, 'active': link.active}"
                    >
                        <a
                            class="page-link"
                            :href="link.url ? link.url : '#'"
                            v-html="link.label"
                            @click.prevent="listenSelectPagination(link.label)"
                        ></a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>

</script>

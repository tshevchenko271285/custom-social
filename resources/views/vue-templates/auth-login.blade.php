<script type="text/x-template"  id="auth-login-template">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('api.login') }}" @submit.prevent="submitForm">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input
                                    v-model="email"
                                    id="email"
                                    type="email"
                                    class="form-control"
                                    :class="{'is-invalid': errors.email}"
                                    required
                                    autocomplete="email"
                                    autofocus
                                />
                                <span v-if="errors.email" class="invalid-feedback" role="alert">
                                    <strong>{# errors.email #}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input
                                    v-model="password"
                                    id="password"
                                    type="password"
                                    class="form-control"
                                    :class="{'is-invalid': errors.password}"
                                    required
                                    autocomplete="current-password"
                                />
                                <span v-if="errors.password" class="invalid-feedback" role="alert">
                                    <strong>{# errors.password #}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4 d-flex justify-content-between">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('register'))
                                    <a class="btn btn-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</script>

<script type="text/x-template"  id="auth-register-template">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('api.register') }}" @submit.prevent="submitForm">

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input
                                    v-model="name"
                                    id="name"
                                    type="text"
                                    class="form-control"
                                    :class="{'is-invalid': errors.name}"
                                    name="name"
                                    required
                                    autocomplete="name"
                                    autofocus
                                />

                                <span v-if="errors.name" class="invalid-feedback" role="alert">
                                    <strong>{# errors.name[0] #}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input
                                    v-model="email"
                                    id="email"
                                    type="email"
                                    class="form-control"
                                    :class="{'is-invalid': errors.email}"
                                    name="email"
                                    required
                                    autocomplete="email"
                                />

                                <span v-if="errors.email" class="invalid-feedback" role="alert">
                                    <strong>{# errors.email[0] #}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input
                                    v-model="password"
                                    id="password"
                                    type="password"
                                    class="form-control"
                                    :class="{'is-invalid': errors.password}"
                                    name="password"
                                    required
                                    autocomplete="new-password"
                                />

                                <span v-if="errors.password" class="invalid-feedback" role="alert">
                                    <strong>{#errors.password[0] #}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input
                                    v-model="password_confirmation"
                                    id="password-confirm"
                                    type="password"
                                    class="form-control"
                                    :class="{'is-invalid': errors.password}"
                                    name="password_confirmation"
                                    required
                                    autocomplete="new-password"
                                />
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4 d-flex justify-content-between">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</script>

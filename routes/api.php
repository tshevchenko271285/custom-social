<?php

use App\Events\ProfileNotification;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CommentController;
use App\Http\Controllers\Api\PostController;
use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Auth\ApiAuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->name('api.')->group(function () {
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');

    Route::get('users/current', [UserController::class, 'getCurrentUser']);
    Route::post('users/avatar', 'App\\Http\\Controllers\\Api\\UserController@saveAvatar');
    Route::delete('users/{id}/avatar', 'App\\Http\\Controllers\\Api\\UserController@removeAvatar');
    Route::apiResource('users', 'App\\Http\\Controllers\\Api\\UserController')->only(['update']);

    Route::apiResource('posts', PostController::class)->only(['edit', 'update', 'store']);

    Route::patch('comments/approve', [CommentController::class, 'approve'])->name('comments.approve');
    Route::apiResource('comments', CommentController::class)->only(['store', 'update', 'destroy']);
});

Route::post('login', [AuthController::class, 'login'])->name('api.login');
Route::post('register', [AuthController::class, 'register'])->name('api.register');

Route::get('menu', function () {
    return view('components.main-menu');
});

Route::apiResource('users', 'App\\Http\\Controllers\\Api\\UserController')->only(['index']);

Route::apiResource('skills', 'App\\Http\\Controllers\\Api\\SkillController')->only(['index']);
Route::apiResource('educations', 'App\\Http\\Controllers\\Api\\EducationController')->only(['index']);
Route::get('roles', [RoleController::class, 'getAll']);

Route::apiResource('posts', PostController::class)->only(['index', 'show']);



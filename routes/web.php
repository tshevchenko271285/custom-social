<?php

use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use App\Mail\AdminReport;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function() {
    return redirect()->route('posts.index');
});


Route::resource('users', UserController::class);
Route::resource('posts', PostController::class);

Auth::routes();


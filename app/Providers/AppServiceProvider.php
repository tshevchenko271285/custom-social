<?php

namespace App\Providers;

use App\__Helpers\CheckUser;
use App\Services\CommentService;
use App\Services\Contracts\ICommentService;
use App\Services\Contracts\IEducationService;
use App\Services\Contracts\IPostService;
use App\Services\Contracts\IRoleService;
use App\Services\Contracts\ISkillService;
use App\Services\Contracts\IUserService;
use App\Services\EducationService;
use App\Services\PostService;
use App\Services\RoleService;
use App\Services\SkillService;
use App\Services\UserService;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IUserService::class, UserService::class);
        $this->app->bind(ISkillService::class, SkillService::class);
        $this->app->bind(IEducationService::class, EducationService::class);
        $this->app->bind(IPostService::class, PostService::class);
        $this->app->bind(ICommentService::class, CommentService::class);
        $this->app->bind(IRoleService::class, RoleService::class);
//        $this->app->bind('CheckUser', CheckUser::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();

//        DB::listen(function ($query) {
//            dump($query->sql);
//            // $query->sql
//            // $query->bindings
//            // $query->time
//        });
    }
}

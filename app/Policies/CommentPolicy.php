<?php

namespace App\Policies;

use App\Facades\CheckUser;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;

    public function update(User $user, Comment $comment)
    {
        return CheckUser::hasRole(['admin'], $user)
            || $comment->user_id === $user->id;
    }

    public function delete(User $user, Comment $comment)
    {
        return
            $user->can('update', $comment->post)
            || $comment->user_id === $user->id;
    }
}

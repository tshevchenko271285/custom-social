<?php

namespace App\Jobs;

use App\Mail\AdminReport;
use App\Models\Role;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendAdminReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Пользователей с опытом меньше года которые не оставили ни одного комментария
        $usersCount = User::doesntHave('comment')->whereHas('profile', function (Builder $query) {
            $query->where('experience', '<', '1');
        })->count();

        $admins = Role::where('slug', 'admin')->first()->users;

        foreach ($admins as $admin) {
            Mail::to($admin)->send(new AdminReport($usersCount));
        }
        Log::debug(self::class . date('j m Y'));
    }
}

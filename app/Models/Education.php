<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Education extends Model
{
    use HasFactory;

    protected $table = 'educations';

    protected $fillable = ['title', 'description'];

    public function profile(): HasMany
    {
        return $this->hasMany(Profile::class);
    }
}

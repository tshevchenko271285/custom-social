<?php

namespace App\Facades;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Facade;

class CheckUser extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'CheckUser';
    }

    public static function hasRole(array $roles, ?User $user = null): bool
    {
        $result = false;
        $user = $user ?: Auth::user();

        if($user) {
            $userRoles = $user->roles->pluck('slug')->toArray();
            foreach ($roles as $role) {
                if($result = in_array($role, $userRoles)) {
                    break;
                }

            }
        }

        return $result;
    }
}

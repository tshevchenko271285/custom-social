<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check() && $this->user()->can('update', $this->route('user'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'user.name' => ['required', 'min:3'],
            'user.email' => ['required', 'email', 'unique:users,id,' . $this->user['email']],
            'profile.experience' => ['integer', 'min:0', 'max:99'],
            'user.password' => ['nullable', 'min:3', 'confirmed'],
            'roles' => ['array'],
        ];
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostController extends Controller
{

    public function index()
    {
        return view('post.index');
    }

    public function create(Request $request)
    {
        return view('post.create');
    }

    public function show($id)
    {
        return view('post.show', ['postId' => $id]);
    }

    public function edit(Request $request, $postId)
    {
        return view('post.update', ['postId' => $postId]);
    }

}

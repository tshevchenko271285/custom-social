<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SkillResource;
use App\Services\Contracts\ISkillService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class SkillController extends Controller
{

    protected ISkillService $skillService;

    public function __construct(ISkillService $skillService)
    {
        $this->skillService = $skillService;
    }

    public function index()
    {
        return SkillResource::collection($this->skillService->getAll());
    }
}

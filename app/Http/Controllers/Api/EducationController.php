<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\EducationResource;
use App\Services\Contracts\IEducationService;

class EducationController extends Controller
{
    protected IEducationService $educationService;

    public function __construct(IEducationService $educationService)
    {
        $this->educationService = $educationService;
    }

    public function index()
    {
        return EducationResource::collection($this->educationService->getAll());
    }
}

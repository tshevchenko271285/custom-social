<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Comment\ApproveCommentRequest;
use App\Http\Requests\Comment\CreateCommentRequest;
use App\Http\Requests\Comment\UpdateCommentRequest;
use App\Http\Resources\CommentResource;
use App\Services\Contracts\ICommentService;

class CommentController extends Controller
{

    protected ICommentService $commentService;

    public function __construct(ICommentService $commentService)
    {
        $this->commentService = $commentService;
    }

    public function store(CreateCommentRequest $request)
    {
        $this->commentService->create($request->only(['text', 'post_id']));

        return response(null, 200);
    }

    public function update(UpdateCommentRequest $request, $id)
    {
        $this->commentService->update($request->only(['text']), $id);

        return response(null, 200);
    }

    public function destroy($id)
    {
        $this->commentService->remove($id);

        return response(null, 200);
    }

    public function approve(ApproveCommentRequest $request) {
        $comment = $this->commentService->approve($request->only(['comment']));

        return CommentResource::make($comment);
    }

}

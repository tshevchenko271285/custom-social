<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Services\Contracts\IUserService;
use Illuminate\Http\Request;

class AuthController extends Controller
{

    protected $userService = null;

    public function __construct(IUserService $userService)
    {
        $this->userService = $userService;
    }

    public function login(LoginRequest $request)
    {
        return $this->userService->login($request->all());
    }

    public function register(RegisterRequest $request)
    {
        return $this->userService->register($request->all());
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response(null, 200);
    }
}

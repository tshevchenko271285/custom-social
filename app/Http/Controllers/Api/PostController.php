<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\CreatePostRequest;
use App\Http\Requests\Post\EditPostRequest;
use App\Http\Requests\Post\UpdatePostRequest;
use App\Http\Resources\PostResource;
use App\Services\Contracts\IPostService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{

    protected IPostService $postService;

    public function __construct(IPostService $postService)
    {
        $this->postService = $postService;
    }

    public function index()
    {
        $posts = $this->postService->getAll();

        return PostResource::collection($posts);
    }

    public function store(CreatePostRequest $request)
    {
        $post = $this->postService->create($request->all());

        return PostResource::make($post);
    }

    public function show($id)
    {
        return PostResource::make($this->postService->getById($id));
    }

    public function edit(EditPostRequest $request, $id)
    {
        return PostResource::make($this->postService->getById($id));
    }

    public function update(UpdatePostRequest $request, $id)
    {
        return PostResource::make($this->postService->update($id, $request->all()));
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\RemoveUserAvatarRequest;
use App\Http\Requests\User\SaveUserAvatarRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Services\Contracts\IUserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    protected IUserService $userService;

    public function __construct(IUserService $userService)
    {
        $this->userService = $userService;
    }

    public function index(Request $request)
    {
        $users = $this->userService->getAll($request->all());

        return UserResource::collection($users);
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        return UserResource::make($this->userService->update($request->all(), $user));
    }

    public function saveAvatar(SaveUserAvatarRequest $request)
    {
        $this->userService->saveAvatar($request->all());
    }

    public function removeAvatar(RemoveUserAvatarRequest $request, $userId)
    {
        $this->userService->removeAvatar($userId);
    }

    public function getCurrentUser()
    {
        return UserResource::make($this->userService->getUser(Auth::id()));
    }
}

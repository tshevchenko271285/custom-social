<?php

namespace App\Http\Resources;

use App\Facades\CheckUser;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $hasEdit = CheckUser::hasRole(['admin'], $request->user('api'))
            || ($request->user('api') && $this->id === $request->user('api')->id);

        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'avatar' => $this->profile->avatar ? asset($this->profile->avatar) : 'https://iupac.org/wp-content/uploads/2018/05/default-avatar.png',
            'education' => EducationResource::make($this->profile->education),
//            'education' => EducationResource::make($this->whenLoaded('profile.education')),
            'experience' => $this->profile->experience,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'skills' => SkillResource::collection($this->whenLoaded('skills')),
//            'skills' => SkillResource::collection($this->skills),
            'hasEdit' => $hasEdit,
//            'roles' => RoleResource::collection($this->roles),
            'roles' => RoleResource::collection($this->whenLoaded('roles')),
        ];
    }
}

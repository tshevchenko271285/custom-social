<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $canEdit = Auth::guard('api')->check()
            && Auth::guard('api')->user()->can('update', $this->resource);

        return [
            'id' => $this->id,
            'title' => $this->title,
            'url' => route('posts.show', ['post' => $this->id]),
            'url_edit' => $canEdit ? route('posts.edit', ['post' => $this->id]) : '',
            'description' => $this->description,
            'content' => $this->content,
            'created_at' => $this->created_at->format('Y-m-j H:i'),
            'updated_at' => $this->updated_at->format('Y-m-j H:i'),
            'education' => EducationResource::make($this->education),
            'user' => UserResource::make($this->whenLoaded('user')),
            'comments' => CommentResource::collection($this->whenLoaded('comments')),
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        $canEdit = Auth::guard('api')->check() && Auth::guard('api')->user()->can('update', $this->resource);
        $canApproved = Auth::guard('api')->check() && Auth::guard('api')->user() ->can('update', $this->resource->post);
        $canRemove = Auth::guard('api')->check() && Auth::guard('api')->user()->can('delete', $this->resource);

        return [
            'id' => $this->id,
            'text' => $this->text,
            'approved' => $this->approved,
            'created_at' => $this->created_at,
            'user' => UserResource::make($this->whenLoaded('user')),
            'update_url' => $canEdit ? route('api.comments.update', $this->resource) : '',
            'approve_url' => $canApproved ? route('api.comments.approve') : '',
            'remove_url' => $canRemove ? route('api.comments.destroy', $this->resource) : '',
        ];
    }
}

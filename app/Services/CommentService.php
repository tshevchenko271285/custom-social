<?php

namespace App\Services;

use App\Models\Comment;
use App\Services\Contracts\ICommentService;
use Illuminate\Support\Facades\Auth;

class CommentService implements ICommentService
{

    public function create(array $data): Comment
    {
        return Auth::user()->comment()->create($data);
    }

    public function update($data, $id): Comment
    {
        $comment = Comment::find($id);
        $comment->text = $data['text'];
        $comment->save();

        return $comment;
    }

    public function remove($id): bool
    {
        $comment = Comment::with(['post'])->find($id);

        return $comment->delete();
    }

    public function approve($id): Comment
    {
        $comment = Comment::with(['post'])->where('id', $id)->first();
        $comment->approved = !$comment->approved;
        $comment->save();

        return $comment;
    }

}

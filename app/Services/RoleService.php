<?php

namespace App\Services;

use App\Models\Role;
use App\Services\Contracts\IRoleService;

class RoleService implements IRoleService
{
    public function getAll()
    {
        return Role::All();
    }
}

<?php
namespace App\Services;

use App\Models\Education;
use App\Services\Contracts\IEducationService;
use Illuminate\Database\Eloquent\Collection;

class EducationService implements IEducationService {

    public function getAll(): Collection
    {
        return Education::all();
    }
}

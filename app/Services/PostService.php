<?php
namespace App\Services;

use App\Models\Post;
use App\Services\Contracts\IPostService;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PostService implements IPostService
{

    public function getAll(): LengthAwarePaginator
    {
        return Post::with(['user', 'user.profile', 'user.roles', 'user.profile.education'])
            ->orderByDesc('id')
            ->paginate(Post::PER_PAGE);
    }

    public function getById(int $id): Post
    {
        $post = Post::with(['comments', 'user', 'comments.user'])->find($id);

        if(!Auth::guard('api')->check() || !Auth::guard('api')->user()->can('update', $post)) {
            $post->comments = $post->comments->filter(fn($comment) => $comment->approved === 1);
        }

        return $post;
    }

    public function create(array $data): Post
    {
        return Post::create([
            'title' => $data['title'],
            'content' => $data['content'],
            'user_id' => Auth::id(),
        ]);
    }

    public function update(int $id, array $data): Post
    {
        $post = Post::find($id);

        $post->update([
            'title' => $data['title'],
            'content' => $data['content']
        ]);

        return $post;
    }

    public function delete(int $id): void
    {
        $post = Post::find($id);

        $message = _('Post ') . '"<b>' . $post->title . '</b>"' . _(' has been deleted!');

        if($post->delete()) {
            Session::flash('news-notification', $message);
        }
    }
}

<?php
namespace App\Services;

use App\Facades\CheckUser;
use App\Models\User;
use App\Notifications\ProfileChanged;
use App\Services\Contracts\IUserService;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserService implements IUserService {

    public function getUser($id)
    {
        return User::with(['profile', 'skills', 'profile.education', 'roles'])->find($id);
    }

    public function getAll($data = [])
    {
        if($data) {

            $query = User::with(['profile', 'profile.education', 'skills', 'roles']);

            // Date
            if(!empty($data['startDate'])) {
                $query->where('created_at', '>=', $data['startDate']);
            }
            if(!empty($data['endDate'])) {
                $query->where('created_at', '<=', $data['endDate']);
            }

            // Skills
            if(!empty($data['skills'])) {
                $query->whereHas('skills', function($_query) use ($data) {
                    $_query->whereIn('skills.id', $data['skills']);
                });
            }

            if(!empty($data['education'])) {
                $query->whereHas('profile.education', function($_query) use ($data) {
                    $_query->where('educations.id', $data['education']);
                });
            }

            $query->orderBy($data['sortBy'], $data['sortDirection']);

            $result = $query->paginate(User::PER_PAGE);

        } else {
            $result = User::with(['profile', 'skills'])->paginate(User::PER_PAGE);
        }

        return $result;
    }

    public function create($userData): User
    {
        $userData['password'] = Hash::make($userData['password']);
        $user = User::create($userData);
        $user->profile()->create();

        return $user;
    }

    public function update(array $data, User $user): User
    {
        $userData = $data['user'];
        $profileData = $data['profile'];
        $skills = $data['skills'];

        if(isset($userData['password']) && $userData['password']) {
            $userData['password'] = Hash::make($userData['password']);
        } else {
            unset($userData['password']);
        }

        $user->profile()->update($profileData);
        $user->skills()->sync($skills);

        $user->update($userData);
        $user->notify(new ProfileChanged());

        if(CheckUser::hasRole(['admin'])) {
            $user->roles()->sync($data['roles']);
        }

        return $user;
    }

    public function removeUser(User $user): bool
    {
        return $user->delete();
    }

    public function saveAvatar($data): string
    {
        $user = $this->getUser($data['user_id']);
        $path = $data['avatar']->store('avatars');
        if($user->profile->avatar) {
            Storage::delete($user->profile->avatar);
        }
        $user->profile->avatar = $path;
        $user->push();

        return $path;
    }

    public function removeAvatar($userId)
    {
        $user = $this->getUser($userId);
        $user->profile->avatar = null;
        $user->profile->save();
    }

    public function login(array $data): Response
    {
        $user = User::where('email', $data['email'])->first();

        if (Hash::check($data['password'], $user->password)) {
            $token = $user->createToken($user->tokenName)->accessToken;

            return response(['token' => $token], 200);
        } else {
            $response = ["errors" => [
                'password' => 'Password mismatch',
            ]];

            return response($response, 422);
        }
    }

    public function register(array $data): Response
    {
        $user = $this->create($data);
        $token = $user->createToken($user->tokenName)->accessToken;

        return response(['token' => $token], 200);
    }
}

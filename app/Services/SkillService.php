<?php
namespace App\Services;

use App\Models\Skill;
use App\Services\Contracts\ISkillService;
use Illuminate\Database\Eloquent\Collection;

class SkillService implements ISkillService {

    public function getAll(): Collection
    {
        return Skill::all();
    }
}

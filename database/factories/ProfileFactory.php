<?php

namespace Database\Factories;

use App\Models\Education;
use App\Models\Profile;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProfileFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected string $model = Profile::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'education_id' => Education::all()->random(),
            'experience' => $this->faker->numberBetween(1, 35),
        ];
    }
}

<?php

namespace Database\Seeders;

use App\Models\Education;
use Illuminate\Database\Seeder;

class EducationSeeder extends Seeder
{
    const EDUCATIONS_LIST = ['Начальное образование', 'Среднее образование', 'Высшее образование'];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::EDUCATIONS_LIST as $education) {
            $model = Education::create([
                'title' => $education
            ]);
            $model->save();
       }
    }
}

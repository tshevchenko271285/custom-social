<?php

namespace Database\Seeders;

use App\Models\Education;
use App\Models\Role;
use App\Models\User;
use App\Models\Profile;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createManagers();

        User::factory()
            ->count(25)
            ->has(Profile::factory())
            ->create();
    }

    protected function createManagers(): void
    {
        $educations = Education::all();
        $adminRole = Role::where('slug', Role::ADMIN)->first();
        $editorRole = Role::where('slug', Role::EDITOR)->first();

        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@mail.com',
            'password' => Hash::make('123'),
        ]);
        $admin->roles()->attach($adminRole);
        $admin->profile()->create([
            'education_id' => $educations->random()->id,
            'experience' => 99,
        ]);

        $editor = User::create([
            'name' => 'Editor',
            'email' => 'editor@mail.com',
            'password' => Hash::make('123'),
        ]);
        $editor->roles()->attach($editorRole);
        $editor->profile()->create([
            'education_id' => $educations->random()->id,
            'experience' => 99,
        ]);
    }
}

<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Post;
use App\Models\Profile;
use Database\Factories\ProfileFactory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            EducationSeeder::class,
            RoleSeeder::class,
            UserSeeder::class,
            PostSeeder::class,
            CommentSeeder::class,
            SkillSeeder::class,
            UserSkillsSeeder::class,
        ]);
    }
}

<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        foreach (Post::all() as $post) {
            Comment::factory()
                ->count(5)
                ->state(new Sequence(
                    fn () => [
                        'user_id' => $users->random(),
                        'post_id' => $post->id,
                    ]
                ))->create();
        }

    }
}
